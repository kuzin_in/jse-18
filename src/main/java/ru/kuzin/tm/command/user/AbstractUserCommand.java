package ru.kuzin.tm.command.user;

import ru.kuzin.tm.api.service.IAuthService;
import ru.kuzin.tm.api.service.IUserService;
import ru.kuzin.tm.command.AbstractCommand;
import ru.kuzin.tm.exception.entity.UserNotFoundException;
import ru.kuzin.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Override
    public String getArgument() {
        return null;
    }

}

