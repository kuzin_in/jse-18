package ru.kuzin.tm.command.user;

import ru.kuzin.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    private final String NAME = "view-user-profile";

    private final String DESCRIPTION = "view profile of current user";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());

    }

}