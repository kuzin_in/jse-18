package ru.kuzin.tm.command.project;

import ru.kuzin.tm.api.service.IProjectService;
import ru.kuzin.tm.api.service.IProjectTaskService;
import ru.kuzin.tm.command.AbstractCommand;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }


    @Override
    public String getArgument() {
        return null;
    }

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

}