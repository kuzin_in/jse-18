package ru.kuzin.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported!");
    }

    public CommandNotSupportedException(final String argument) {
        super("Error! Command ``" + argument + "`` not supported!");
    }

}
